from PaxcelChatBotFramework.Models import Output
def formResponse(input, context):
    output = Output()
    output.Type = "form"
    output.Data = {
        "name": input["name"]
    }
    result = {
        "action": "return",
        "output": output
    }
    return result
