import logging
logger = logging.getLogger(__name__)
from PaxcelChatBotFramework.Models import Output
from RuleEngine import StringBuilder
# def StringBuilder(stringformat, outputDict):
#     try:
#         words = stringformat.split(" ")
#         while("" in words):
#             words.remove("")
#         compiledWords = []
#         sentenceEndChar = [",", "!"]
#         for word in words:
#             filterWord = None
#             deliminator = None
#             if word[-1] in sentenceEndChar:
#                 filterWord = word[0:-1]
#                 deliminator = word[-1]
#             else:
#                 filterWord = word
#
#             if filterWord in outputDict:
#                 result = outputDict[filterWord]
#                 if deliminator:
#                     result += deliminator
#                     compiledWords.append(str(result))
#             else:
#                 compiledWords.append(str(filterWord))
#         string = " ".join(compiledWords)
#         logger.info(
#         "User Query: {0}".format("output from stringBuilder is:"+str(string))
#         )
#     except:
#         logger.info(
#         "User Query: {0}".format("exception raise in StringBuilder")
#         )
#         raise
#     return string

def textResponse(input, context):
    result = {
        "action": "return"
    }
    output = Output()
    output.Type = "text"
    output.Message = StringBuilder(input, context)
    result["output"] = output
    return result




