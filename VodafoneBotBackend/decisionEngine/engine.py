from RuleEngine import Engine as RuleExecutor
from RuleEngine import ActionManager, ResponseManager
from PaxcelChatBotFramework import Engine as ChatBotRuleEngine
from .contextBuilder import ContextBuilder
from .ruleManager import RuleManager
from PaxcelChatBotFramework.Models import RuleEngineOutput, VisitedNode, Context, Output
from PaxcelChatBotFramework.Enums import RedirectType
from .response import *
from .actions import *
import datetime
import logging
logger = logging.getLogger(__name__)


class DecisionEngine(ChatBotRuleEngine):
    @staticmethod
    def Initialize():
        ResponseManager.add_action("text", textResponse)
        ResponseManager.add_action("options", optionResponse)
        ResponseManager.add_action("form", formResponse)
        ResponseManager.add_action("conditional", conditionalResponse)
        ResponseManager.add_action("jumpto", jumptoResponse)
        ActionManager.add_action("addToContext", addToContextAction)
        ActionManager.add_action("addEntitytocontext", addEntitytocontext)
        ActionManager.add_action("regularUpdate", regularUpdate)
        ActionManager.add_action("RequestCallBackByPhoneNo", RequestCallBackByPhoneNo)
        #OTP 
        ActionManager.add_action("getotpmsg", getotpmsg)
        ActionManager.add_action("verifyotpmsg", verifyotpmsg)
        ActionManager.add_action("getRecommendedScholarships", getRecommendedScholarships)
        ActionManager.add_action("getByIdAndLanguage", getByIdAndLanguage)
        ActionManager.add_action("userRegistration", userRegistration)
        ActionManager.add_action("exploreMore", exploreMore)
        ActionManager.add_action("getScholarshipLink",getScholarshipLink)
        ActionManager.add_action("profileCompletion",profileCompletion)
        ActionManager.add_action("getProfileLink",getProfileLink)
        ActionManager.add_action("getMatchedScholarships",getMatchedScholarships)
        ActionManager.add_action("getScholarshipByFilters",getScholarshipByFilters)
        ActionManager.add_action("addToFavouriteScholarships",addToFavouriteScholarships)
        ActionManager.add_action("getRecommendedScholarshipsByMobile",getRecommendedScholarshipsByMobile)
        ActionManager.add_action("submitquery",submitquery)
        ActionManager.add_action("submit_empty_query",submit_empty_query)
        ActionManager.add_action("get_queries",get_queries)
        ActionManager.add_action("request_callback_by_mobile",request_callback_by_mobile)
        ActionManager.add_action("request_callback_by_email",request_callback_by_email)
        ActionManager.add_action("saveAppliedScholarships",saveAppliedScholarships)
        ActionManager.add_action("getAppliedScholarshipsList",getAppliedScholarshipsList)
        ActionManager.add_action("getScholarshipsForClaim", getScholarshipsForClaim)
        ActionManager.add_action("addUploadedDocument",addUploadedDocument)
        ActionManager.add_action("getAppliedAndFavouriteScholarshipsForClaim",getAppliedAndFavouriteScholarshipsForClaim)


    def __init__(self):
        pass

    def Process(self, botResponse):
        print(botResponse)
        try:
            start = datetime.datetime.now()
            print(botResponse.Intent)
            dbConfig = self.__fetchDBValues__(botResponse.AdditionalData)
            ruleLoader = RuleManager(dbConfig)
            ruleExec = RuleExecutor(ruleLoader)
            contextTable = ContextBuilder(botResponse)
            resp = ruleExec.Execute(contextTable)

            result = RuleEngineOutput()
            result.AdditionalData = botResponse.AdditionalData
            result.Context = self.__prepareContext__(
                botResponse.Context, resp["context"])
            result.Output = self.__prepareOutput__(resp["output"])
            stop = datetime.datetime.now()
            totalTime = "Total time used by rule engine:" + str(stop - start)
            logger.info(
                "User Query: {0}".format(totalTime)
            )
        except:
            logger.info(
                "User Query: {0}".format("Exception occurd in rule engine")
            )
            raise
        return result

    def __fetchDBValues__(self, additionalData):
        config = {}
        print(additionalData)
        config = {"dbName": additionalData["dbName"], "message": additionalData["message"],
                  "uuid": additionalData["uuid"], "logId": additionalData["logId"]}
        print(config, "configuraton property")
        return config

    def __prepareContext__(self, oldContext, engineContext):
        nodes = engineContext["nodes"]
        oldContext.Properties = {}
        oldContext.VisitedNodes = []
        for node in nodes:
            vnode = VisitedNode()
            if isinstance(node, VisitedNode):
                vnode = node
                vnode.RedirectType = self.__getRedirectType__(
                    vnode.RedirectType)
            else:
                vnode.NodeId = node["nodeid"]
                vnode.RedirectType = self.__getRedirectType__(
                    node["redirectType"])

            oldContext.VisitedNodes.append(vnode)

        for prop in engineContext:
            oldContext.Properties[prop] = engineContext[prop]
        return oldContext

    def __getRedirectType__(self, redirectType):
        if redirectType == "slot":
            return RedirectType.SLOT
        # if redirectType == "return":
        return RedirectType.RESPONSE

    def __prepareOutput__(self, responses):
        result = []
        for response in responses:
            result.append(response)

        return result
