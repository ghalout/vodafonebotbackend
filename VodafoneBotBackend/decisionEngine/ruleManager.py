from RuleEngine.Components import RuleLoader
from dataAccessLayer.dbHandler import getRules, getRuleById, getAnyThingElseNode, hasChildNode


class RuleManager(RuleLoader):
    def __init__(self, config):
        self.config = config

    def getRules(self, parentId) -> list:
        return getRules(parentId, self.config["dbName"])

    def getRuleById(self, id):
        return getRuleById(id, self.config["dbName"])

    def getAnyThingElseNode(self):
        print(self.config)
        # return getAnyThingElseNode(self.config["dbName"],self.config["message"]["message"],self.config["uuid"],self.config["logId"])

        return getAnyThingElseNode(self.config["dbName"], "Anything else", self.config["uuid"],
                               self.config["logId"])

    def hasChildNode(self, parentId):
        return hasChildNode(parentId, self.config["dbName"])
