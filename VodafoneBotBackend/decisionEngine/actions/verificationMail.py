#
# import requests
# import json
#
# headerValue = {'content-Type': 'application/json'}
#
#
# def sendverificationmail(user, userName, data):
#     url='http://100.25.71.230:3005/api/mail/send'
#     key = (data._id + "1" + data.academicDetails._id + "2" + data.familyDetails._id + "3" + data.personalDetails._id).toString().replace('5', 'a')
#     myURL = "100.25.71.230:4005/home" + "/verifyUser/" + key + "/" + user
#     subject = 'Verification Email From Learning With Vodafone'
#     html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml">' \
#            ' <head> <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> <meta name="viewport" content="width=device-width, initial-scale=1.0"> <title>Untitled Document</title> </head> ' \
#            '<body topmargin="0" leftmargin="0"> <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">' \
#            ' <tr> <td width="600" height="295" colspan="3" valign="bottom"><img src="https://learningwithvodafone.s3.ap-south-1.amazonaws.com/images/top-bg-new-vf-new.jpg" width="600" height="300" alt="Vodafone" /> </td> </tr>' \
#            ' <tr> <td width="20" height="30" rowspan="6" bgcolor="#f47920"></td> <td width="560" height="15" bgcolor="#f47920"></td> <td width="20" rowspan="6" bgcolor="#f47920"></td> </tr>' \
#            ' <tr> <td height="15" bgcolor="#f47920"><span style="font:500 24px Arial,Helvetica,sans-serif;color:#ffffff">Learning with Vodafone Idea- Account Verification</span></td> </tr>' \
#            ' <tr> <td height="20" align="center" valign="middle" bgcolor="#f47920"><span style="font:500 16px Arial,Helvetica,sans-serif;color:#ffffff">You are almost there!</span></td> </tr>' \
#            ' <tr> <td height="5" align="center" valign="middle" bgcolor="#f47920"></td> </tr>' \
#            ' <tr> <td height="80" bgcolor="#f47920"><span style="font:15px/19px Arial,Helvetica,sans-serif;color:#ffffff;">Dear '+userName+' <br /> <span style="font: 15px/19px Arial,Helvetica,sans-serif; color: #ffffff;">Thank you for signing in to Learning with Vodafone Idea Scholarship Platform. You are one step away from exploring and applying to more than 100 scholarships offered on our platform.</span> </span></td> </tr>' \
#           ' <tr> <td height="5" bgcolor="#f47920"></td> </tr> <tr> <td width="20" height="30" rowspan="5" bgcolor="#f47920"></td> <td width="560" height="5" align="center" valign="middle" bgcolor="#f47920"></td> <td width="20" rowspan="5" bgcolor="#f47920"></td> </tr>' \
#           ' <tr> <td height="25" align="left" valign="middle" bgcolor="#f47920"><span style="font:15px/19px Arial,Helvetica,sans-serif;color:#ffffff;">Please confirm your email address by clicking on the \'Activate Account\' button below</span>:</td> </tr>' \
#           ' <tr> <td height="45" align="center" valign="middle" bgcolor="#f47920"> <a href=' + myURL + ' style="color:#ffffff; text-decoration:none;"> <table width="200" bgcolor="#7030a0" border="0" cellspacing="0" cellpadding="0" style="border-radius: 5px;"> <tr> <td height="40" align="center" valign="middle"><span style="font:500 18px Arial,Helvetica,sans-serif;color:#ffffff">Activate Account</span></td> </tr> </table> </a> </td> </tr>' \
#           ' <tr> <td height="25" align="left" valign="middle" bgcolor="#f47920"><span style="font:15px/19px Arial,Helvetica,sans-serif;color:#ffffff;">If you received this by mistake or weren\'t expecting it please discard this email.</span></td> </tr>' \
#           ' <tr> <td height="15" align="center" valign="middle" bgcolor="#f47920"></td> </tr> <tr> <td width="20" height="10" rowspan="5" bgcolor="#f4f5f7"></td> <td width="560" height="10" align="center" valign="middle" bgcolor="#f4f5f7"><span style="color:#363435; font-size:16px;"><span style="font-size:18px; font-weight:bold;"> <br />Need Help! </span> <br /> <br /> In Case this link does not work, please copy paste this URL on your browser: </span> </td> <td width="20" rowspan="5" bgcolor="#f4f5f7"></td> </tr>' \
#           ' <tr> <td height="25" align="center" valign="middle" bgcolor="#f4f5f7"><span style="font:500 27px \'Open Sans\', Arial, Helvetica, sans-serif; color:#ffffff;"> <textarea name="textarea" readonly="readonly" style="width:100%;height:auto;border:0;outline:0;background-color:#f4f5f7;color:#f47920;font-size:16px">' + myURL + '</textarea> </span></td> </tr>' \
#           ' <tr> <td height="25" align="center" valign="middle" bgcolor="#f4f5f7"><span style="color:#363435; font-size:16px;">If you are having difficulties activating your Account, then please contact us at <br /> <a href="mailto:support@learningwithvodafoneidea.in?Subjec=" style="color:#f47920;font-weight:500" target="_blank">support@learningwithvodafoneidea.in</a> </span></td> </tr>' \
#           ' <tr> <td height="25" align="center" valign="bottom" bgcolor="#f4f5f7"><span style="color:#727377; font-size:12px;">This message comes from an unmonitored mailbox. Please do not reply to this message.</span></td> </tr> <tr> <td height="25" bgcolor="#f4f5f7"></td> </tr> </table> </body> </html>'
#
#     params = {"email": user,
#               "subject": subject,
#               "html": html}
#     headers = headerValue
#     response= requests.post(url,data=json.dumps(params), headers=headers)
#     apidata= response.json()
#     data = {
#
#         "$emailverification": apidata["data"],
#
#     }
#     # print(data)
#     return data
#
