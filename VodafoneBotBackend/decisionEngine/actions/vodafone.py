
import requests
import json

headerValue = {'content-Type': 'application/json'}
""" mainURL = 'https://vodafone.toffeeglobal.com:4443/api'
siteURL = 'https://vodafone.toffeeglobal.com' """

mainURL = 'http://100.25.71.230:3005/api'
siteURL = 'http://100.25.71.230:4005'

def userRegistration(input, context):    
    email = context["$email"][0]
    phoneno = context["$phoneno"][0]
    name = context["$name"][0]
    firstname = name.strip().split(' ')[0]
    lastname = ' '.join((name + ' ').split(' ')[1:]).strip()
    password = context["$password"][0]
    keys = context["$emailkey"][0]
    key = keys
    user= email
    username= name
    myURL = siteURL + "/verifyUser/" + key + "/" + user
    subject = 'Verification Email From Learning With Vodafone'
    html = '<!DOCTYPE html     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <html xmlns="http://www.w3.org/1999/xhtml"> <head>     <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />     <meta name="viewport" content="width=device-width, initial-scale=1.0">     <title>Untitled Document</title> </head> <body topmargin="0" leftmargin="0">     <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">         <tr>             <td width="600" height="295" colspan="3" valign="bottom"><img                     src="https://learningwithvodafone.s3.ap-south-1.amazonaws.com/images/top-bg-new-vf-new.jpg"                     width="600" height="300" alt="Vodafone" />             </td>         </tr>         <tr>             <td width="20" height="30" rowspan="6" bgcolor="#f47920"></td>             <td width="560" height="15" bgcolor="#f47920"></td>             <td width="20" rowspan="6" bgcolor="#f47920"></td>         </tr>         <tr>             <td height="15" bgcolor="#f47920"><span                     style="font:500 24px Arial,Helvetica,sans-serif;color:#ffffff">Learning with Vodafone Idea- Account                     Verification</span></td>         </tr>         <tr>             <td height="20" align="center" valign="middle" bgcolor="#f47920"><span                     style="font:500 16px Arial,Helvetica,sans-serif;color:#ffffff">You are almost there!</span></td>         </tr>         <tr>             <td height="5" align="center" valign="middle" bgcolor="#f47920"></td>         </tr>         <tr>             <td height="80" bgcolor="#f47920"><span                     style="font:15px/19px Arial,Helvetica,sans-serif;color:#ffffff;">Dear '+username+' <br /><span style="font: 15px/19px Arial,Helvetica,sans-serif; color: #ffffff;">Thank you for signing in to Learning with Vodafone Idea Scholarship Platform. You are one step away                     from                     exploring and applying to more than 100 scholarships offered on our platform.</span> </span></td>         </tr>         <tr>             <td height="5" bgcolor="#f47920"></td>         </tr>         <tr>             <td width="20" height="30" rowspan="5" bgcolor="#f47920"></td>             <td width="560" height="5" align="center" valign="middle" bgcolor="#f47920"></td>             <td width="20" rowspan="5" bgcolor="#f47920"></td>         </tr>         <tr>             <td height="25" align="left" valign="middle" bgcolor="#f47920"><span                     style="font:15px/19px Arial,Helvetica,sans-serif;color:#ffffff;">Please confirm your email address                     by clicking on the \'Activate Account\' button below</span>:</td>         </tr>         <tr>             <td height="45" align="center" valign="middle" bgcolor="#f47920">                 <a href=' + myURL +    ' style="color:#ffffff; text-decoration:none;">                     <table width="200" bgcolor="#7030a0" border="0" cellspacing="0" cellpadding="0"                         style="border-radius: 5px;">                         <tr>                             <td height="40" align="center" valign="middle"><span                                     style="font:500 18px Arial,Helvetica,sans-serif;color:#ffffff">Activate                                     Account</span></td>                         </tr>                     </table>                 </a>             </td>         </tr>         <tr>             <td height="25" align="left" valign="middle" bgcolor="#f47920"><span                     style="font:15px/19px Arial,Helvetica,sans-serif;color:#ffffff;">If you received this by mistake or                     weren\'t expecting it please discard this email.</span></td>         </tr>         <tr>             <td height="15" align="center" valign="middle" bgcolor="#f47920"></td>         </tr>         <tr>             <td width="20" height="10" rowspan="5" bgcolor="#f4f5f7"></td>             <td width="560" height="10" align="center" valign="middle" bgcolor="#f4f5f7"><span                     style="color:#363435; font-size:16px;"><span style="font-size:18px; font-weight:bold;"> <br />Need                         Help! </span>                     <br />                     <br /> In Case this link does not work, please copy paste this URL on your browser: </span>             </td>             <td width="20" rowspan="5" bgcolor="#f4f5f7"></td>         </tr>         <tr>             <td height="25" align="center" valign="middle" bgcolor="#f4f5f7"><span                     style="font:500 27px \'Open Sans\', Arial, Helvetica, sans-serif; color:#ffffff;">                     <textarea name="textarea" readonly="readonly"                         style="width:100%;height:auto;border:0;outline:0;background-color:#f4f5f7;color:#f47920;font-size:16px">' +    myURL + '</textarea>                 </span></td>         </tr>         <tr>             <td height="25" align="center" valign="middle" bgcolor="#f4f5f7"><span                     style="color:#363435; font-size:16px;">If you are having difficulties activating your Account, then                     please contact us at <br />                     <a href="mailto:support@learningwithvodafoneidea.in?Subjec=" style="color:#f47920;font-weight:500"                         target="_blank">support@learningwithvodafoneidea.in</a> </span></td>         </tr>         <tr>             <td height="25" align="center" valign="bottom" bgcolor="#f4f5f7"><span                     style="color:#727377; font-size:12px;">This message comes from an unmonitored mailbox. Please do not                     reply to this message.</span></td>         </tr>         <tr>             <td height="25" bgcolor="#f4f5f7"></td>         </tr>     </table> </body> </html>'

    params = {"email": user,
         "subject": subject,
      "html": html}
    headers = headerValue
    response = requests.post(mainURL +"/mail/send",
                  data=json.dumps(params), headers=headers)
    data = response.json()
    # print("email function call", data)
    return data
# Start Request a callback




def regularUpdate(input, context):

    url = mainURL +'/student/getRecommendedScholarships'
    params = {

        "email": context["$email"][0],


    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    data = response.json()

    return data


def RequestCallBackByPhoneNo(input, context):
    url = mainURL +'/student/getRecommendedScholarships'
    params = {

        "email": context["$phonenumber"][0],


    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    data = response.json()

    return data


def getotpmsg(input, context):
    url = mainURL +'/getOTPFromMSG'
    params = {

        "phoneNo": context["$phoneno"][0],

    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    data = response.json()

    return data


def loginByemail(input, context):
    url = mainURL +'/student/login'
    params = {

        "email": context["$email"][0],

    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    data = response.json()
    return data

# def loginByPhone():
#     url = 'http://100.25.71.230:3005/api/student/phoneLogin'
#     params = {"phone": "8802309196"}
#
#
#
#
#     headers = headerValue
#     response = requests.post(url, data=json.dumps(params), headers=headers)
#
#     data = response.json()
#     print(data)
#     return data


def verifyotpmsg(input, context):

    url = mainURL +'/verifyOtpMSG'
    params = {

        "phoneNo": context["$phoneno"][0],
        "otp": context["$otp"][0]


    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    data = response.json()

    return data


def getByIdAndLanguage(input, context):

    url =mainURL + '/scholarship/getByIdAndLanguage'
    params = {"language": "English"}
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {
        '$scholarship': apidata["data"]
    }
    return data
# End OTP


def getRecommendedScholarships(input, context):

    url = mainURL +'/student/getRecommendedScholarships'
    params = {"email": context["$email"][0]}
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$scholarship": apidata["data"],
        "$url": "https://www.learningwithvodafone.in/en"
    }

    return data


def exploreMore(input, context):
    # print("call explore function")
    url = mainURL +'/utility/getScholarshipFilterList'
    headers = headerValue
    response = requests.get(url, headers)
    apidata = response.json()
    data = {

        "$exploreMore": apidata["data"]
    }

    return data

# def getScholarshipFilters(input,context):
#
#     url = 'http://100.25.71.230:3005/api/utility/getScholarshipFilters'
#
#     headers =headerValue
#     response = requests.get(url, headers=headers)
#
#     apidata = response.json()
#     data = {'$fileterScholerShip':apidata["data"]}
#
#     return data


def profileCompletion(input, context):

    url = mainURL +'/student/login'
    email = context["$email"][0],
    password = "123456",
    params = {
        "email": email,
        "password": password
    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$profile": apidata["data"],
        "$isProfileCompleted": apidata["data"]["isProfileCompleted"]
    }
    return data


def getScholarshipLink(input, context):

    # url = 'http://100.25.71.230:3005/api/scholarship/getByIdAndLanguage'
    # params = {"language":"English"}
    # headers =headerValue
    # response = requests.post(url, data=json.dumps(params), headers=headers)
    #
    # apidata = response.json()
    data = {
        "$url": siteURL+'/'+'en'
    }
    return data


def getProfileLink(input, context):

    url = siteURL +'/'+'myprofile'

    data = {
        "$Profileurl": url
    }
    return data


def getScholarshipByFilters(input, context):
    url = mainURL +'/scholarship/getScholarshipByFilter'
    clas = context["$Class"][0]
    gender = context["$Gender"][0]
    category = context["$Category"][0]
    preference = context["$Preference"][0]
    language = context["$Language"][0]
    if not clas and not gender and not category and not preference:
        url = mainURL +'/scholarship/getByIdAndLanguage'
    #clas = ["1st-10th"]
    # gender =[]
    # category =[]
    # preference =[]
    # language ="English"
    params = {
        "Class": clas,
        "Gender": gender,
        "Category": category,
        "Preference": preference,
        "Language": language
    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    # print(apidata, "TESTING$$$$$$$$$$$$$$SSSSSSSS")
    if "data" in apidata:
        length = len(apidata["data"])
        if length == 0:
            context["$datalength"]=True
        else:
            context["$datalength"] = False
    else:
        context["$datalength"]=True


    data = {

        "$scholarship": apidata["data"],
        "$status_scholarships": apidata["status"],
        "$datalength": str(context["$datalength"])
    }

    return data


def addToFavouriteScholarships(input, context):
    url = mainURL +'/student/addToFavouriteScholarships'
    email = context["$email"][0],
    scholarshipId = context["$scholarshipId"][0],
    params = {
        "email": email,
        "scholarshipId": scholarshipId
    }
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)
    apidata = response.json()
    data = {

        "$favourites": apidata["data"],

    }
    # print(data)
    return data


def getRecommendedScholarshipsByMobile(input, context):

    url =mainURL + '/student/getRecommendedScholarshipsByMobile'
    params = {"mobile": context["$phoneno"][0]}
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()

    data = {

        "$scholarship": apidata["data"],

    }
    return data


def submitquery(input, context):
    url =mainURL + '/student/addQuery'

    email = context["$email"][0]
    phoneno = context["$phoneno"][0]
    query = context["$query"][0]
    # firstname = name.strip().split(' ')[0]
    # lastname = ' '.join((name + ' ').split(' ')[1:]).strip()
    params = {

        "email": email,
        "mobile": phoneno,
        "query": query,
        "source": "Query",
        "communicationMode": "Email",
        "communicationMedium": email,
    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$submitquery": apidata["data"],
        # "$ticketid": apidata["data"]["data"]
        "$ticketid": "AQ123",

    }
    # print(data)
    return data


def submit_empty_query(input, context):
    url = mainURL +'/student/addQuery'

    email = context["$email"][0]
    phoneno = context["$phoneno"][0]
    query = ""
    # firstname = name.strip().split(' ')[0]
    # lastname = ' '.join((name + ' ').split(' ')[1:]).strip()
    params = {

        "email": email,
        "mobile": phoneno,
        "query": query,
        "source": "Query",
        "communicationMode": "Email",
        "communicationMedium": email,
    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$submitquery": apidata["data"],
        # "$ticketid": apidata["data"]["data"]
        "$ticketid": "AQ456",

    }
    return data


def get_queries(input, context):
    # print("call query function")
    url = mainURL +'/master/getMaster?language=English'
    headers = headerValue
    response = requests.get(url, headers)
    apidata = response.json()
    data = {

        "$querylist": apidata["data"]
    }
    return data


def request_callback_by_mobile(input, context):
    url = mainURL +'/student/addQuery'

    email = ""
    phoneno = ""
    query = ""
    communicationmedium = context["$reqphoneno"][0]
    # firstname = name.strip().split(' ')[0]
    # lastname = ' '.join((name + ' ').split(' ')[1:]).strip()

    params = {

        "email": email,
        "mobile": phoneno,
        "query": query,
        "source": "Query",
        "communicationMode": "Mobile",
        "communicationMedium": communicationmedium

    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$requestcallback": apidata["data"],
        "$ticketid": "A123",

    }
    return data


def request_callback_by_email(input, context):
    url = mainURL +'/student/addQuery'

    email = ""
    phoneno = ""
    query = ""
    communicationmedium = context["$reqemail"][0]
    # firstname = name.strip().split(' ')[0]
    # lastname = ' '.join((name + ' ').split(' ')[1:]).strip()

    params = {

        "email": email,
        "mobile": phoneno,
        "query": query,
        "source": "Query",
        "communicationMode": "Email",
        "communicationMedium": communicationmedium

    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$requestcallback": apidata["data"],
        # "$ticketid": apidata["data"]["data"]
        "$ticketid": "A456",

    }
    return data

def getMatchedScholarships(input, context):

    url = mainURL +'/student/getRecommendedScholarships'
    params = {"email": context["$email"][0]}
    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    print(apidata)
    if "data" in apidata:
        length = len(apidata["data"])
        if length == 0:
            context["$datalength"] = True
        else:
            context["$datalength"] = False
    else:
        context["$datalength"] = True
    # print(context["$datalength"])

    data = {

        "$scholarship": apidata["data"],
        "$datalength": str(context["$datalength"]),
    }
    return data


# def saveAppliedScholarships(input, context):
#     url = "http://100.25.71.230:3005/api/activity/saveUserActivity"
#
#     entityid = context["$entityId"][0]
#     sessionid= context["$sessionId"][0]
#
#     params = {
#
#         "eventType": "OK_CHATBOT",
#         "entityName": "ok_chatbot",
#         "entityId": entityid,
#         "sessionId": sessionid,
#         "language":"English"
#
#     }
#
#     headers = headerValue
#     response = requests.post(url, data=json.dumps(params), headers=headers)
#
#     apidata = response.json()
#     data = {
#
#         "$scholarshipsSaved": apidata["data"],
#
#     }
#     print(data)
#     return data




def getAppliedScholarshipsList(input, context):
    url = mainURL +"/scholarship/getScholarshipByIds"

    id = context["$ScholarshipsIdList"][0]


    params = {
        "id": id,
    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()
    data = {

        "$appliedScholarshipsList": apidata["data"],

    }

    return data


def saveAppliedScholarships(input, context):

    url = mainURL +"/activity/saveUserActivity"

    entityid = context["$entityId"][0]
    sessionid= context["$sessionId"][0]
    userId= context["$userId"][0]
    setdata=[]
    for entity in entityid:


        params = {

         "eventType": "OK_CHATBOT",
         "entityName": "ok_chatbot",
         "entityId": entity,
         "sessionId": sessionid,
         "language":"English",
         "userId":userId

         }
        headers = headerValue
        response = requests.post(url, data=json.dumps(params), headers=headers)

        apidata = response.json()
        dataresult = {

        "$scholarshipsSaved": apidata["data"],

         }


        setdata.append(dataresult)
    data = {'$dataScholarshipsSaved':setdata}

    return data



def getScholarshipsForClaim(input, context):

    url =mainURL + '/student/loginByEmail'
    params = {"email": context["$email"][0]}

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)

    apidata = response.json()

    if "ReceivedScholarships" in apidata["data"]["scholarshipHistory"]:
        length = len(apidata["data"]["scholarshipHistory"]["ReceivedScholarships"])
        if length == 0:
            context["$responselength"] = True

        else:
            context["$responselength"] = False

    else:
        context["$responselength"] = True

    # print(context["$responselength"])

    data = {

        "$scholarship": apidata["data"]["scholarshipHistory"]["ReceivedScholarships"],
        "$responselength":str(context["$responselength"]),
    }
    return data


def getAppliedAndFavouriteScholarshipsForClaim(input, context):
    email =  context["$email"][0]
    url = mainURL +'/student/getFavouriteScholarships' + '/' + email
    headers = headerValue
    response = requests.get(url, headers)
    apidata = response.json()

    all_scholarships=[]

    applied_scholarships=apidata["data"][0]["scholarshipHistory"]["ReceivedScholarships"]

    if applied_scholarships is None:
        if applied_scholarships[0]["ScholarshipId"] is not None:
            for count in range(0,len(applied_scholarships)):
                all_scholarships.append(applied_scholarships[count]["ScholarshipId"]["content_by_language"]["English"])

    favourite_scholarships = apidata["data"][0]["scholarshipInterest"]["FavouriteScholarships"]
    if favourite_scholarships:
        for countfav in range(0,len(favourite_scholarships)):            
            all_scholarships.append(favourite_scholarships[countfav]["content_by_language"]["English"])

    name=apidata["data"][0]["personalDetails"]["FirstName"]+ " " + apidata["data"][0]["personalDetails"]["LastName"]

    final_scholarships_applied_favourite={
        'userid': apidata["data"][0]["_id"],
        'name': name,
        'scholarships':all_scholarships
    }


    url2 = mainURL +'/scholarship/getByIdAndLanguage'
    params = {"language": "English"}
    headers = headerValue
    response2 = requests.post(url2, data=json.dumps(params), headers=headers)

    apidata2 = response2.json()

    apidata2_response_array =[]
    for countrandom in range (0, len(apidata2)):
        apidata2_response=apidata2["data"][countrandom]["content_by_language"]["English"]
        apidata2_response_array.append(apidata2_response)

    final_allscholarship ={
        'userid': apidata["data"][0]["_id"],
        'name': name,
        'scholarships':apidata2_response_array
    }
    if len(all_scholarships)==0 and len(apidata2_response_array)==0:
         responselength= True
    else:
        responselength=False


    data = {

        "$userScholarships": final_scholarships_applied_favourite,
        "$allScholarships":final_allscholarship,
        "$responselength":str(responselength),
    }

    return data


def addUploadedDocument(input, context):

    url = mainURL +'/student/saveStudentScholarshipProof'

    userid = context["$uploadparam"][0]["userid"]
    Title=context["$uploadparam"][0]["Title"]
    Year= context["$uploadparam"][0]["Year"]
    Description = context["$uploadparam"][0]["Description"]
    ScholarshipId=context["$uploadparam"][0]["ScholarshipId"]
    DocUrl=context["$uploadparam"][0]["DocUrl"]

    params = {
        "file":{
            "filename": "sample",
            "originalname": "image/png"
        },
        "data":{
            "_id":userid,
            "Title":Title,
            "Year":Year,
            "Description":Description,
            "ScholarshipId":ScholarshipId,
            "DocUrl":DocUrl
        }
    }

    headers = headerValue
    response = requests.post(url, data=json.dumps(params), headers=headers)
    apidata = response.json()
    data = {

        "$uploadreceipt": apidata["data"]

    }
    return data

