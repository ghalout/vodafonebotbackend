from .addToContext import addToContextAction, addEntitytocontext
from .vodafone import regularUpdate ,addToFavouriteScholarships,getAppliedAndFavouriteScholarshipsForClaim,addUploadedDocument,getotpmsg,get_queries,getScholarshipsForClaim,saveAppliedScholarships,submitquery,getScholarshipByFilters,request_callback_by_mobile,request_callback_by_email,getRecommendedScholarshipsByMobile,submit_empty_query ,verifyotpmsg,RequestCallBackByPhoneNo,getMatchedScholarships,profileCompletion,getScholarshipLink,getRecommendedScholarships,getProfileLink,getByIdAndLanguage,userRegistration,exploreMore,getAppliedScholarshipsList
__all__ = [
    "addToContextAction", "addEntitytocontext", "regularUpdate", "getotpmsg","userRegistration" ,"addToFavouriteScholarships",
    "verifyotpmsg", "RequestCallBackByPhoneNo","submitquery","getRecommendedScholarships","get_queries","submit_empty_query",
    "request_callback_by_mobile","getScholarshipByFilters","getRecommendedScholarshipsByMobile","getProfileLink",
    "getMatchedScholarships","getByIdAndLanguage","exploreMore","getScholarshipLink","saveAppliedScholarships","profileCompletion",
    "request_callback_by_email","getAppliedScholarshipsList","getScholarshipsForClaim","getAppliedAndFavouriteScholarshipsForClaim","addUploadedDocument"
]
