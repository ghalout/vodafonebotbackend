from .weboutputProcessor import WebOutputProcessor
from .whatsappoutputProcessor import WhatsappOutputProcessor

__all__ = [
    "WebOutputProcessor",
    "WhatsappOutputProcessor"
]