from PaxcelChatBotFramework import OutputProcessor
from PaxcelChatBotFramework.Models import Response
from dataAccessLayer.dbHandler import insertCHatlogs
import random
import json
import datetime
import logging
from PaxcelChatBotFramework import Program
logger = logging.getLogger(__name__)
class WebOutputProcessor(OutputProcessor):

    @staticmethod
    def CanHandle(request) -> bool:
        return request.AdditionalData["source"] == "web"

    def Process(self, request):
        try:
            start = datetime.datetime.now()
            context = request.Context
            output = request.Output
            additionalData = request.AdditionalData
            dbName = additionalData['dbName']
            response = Response()
            response.Data = {"Output":output,"AdditionalData":additionalData}
            response.Context = context
            jsonOutput = json.dumps(response.__json__(), sort_keys=True, indent=2, separators=(',', ': '))
            OutputJson = json.loads(jsonOutput)
            print(OutputJson, "testing##")

            botMessage = []
            for x in OutputJson["data"]:
                if "option" in x["type"]:
                    botMessage.append(x["data"]["text"])
                elif "text" in x["type"]:
                    botMessage.append(x["message"])
                else:
                    botMessage.append("Form is rendered")

            timeStart = datetime.datetime.now()
            if "conversationId" in OutputJson["context"]:
                conversationId = OutputJson["context"]["conversationId"]
            else:
                value = random.random()
                OutputJson["context"]["conversationId"] = str(value)
                conversationId = str(value)
            for userMessage in botMessage:
                #chatlogsId = insertCHatlogs(conversationId, "98", str(timeStart), userMessage, True, dbName)
                insertCHatlogs(userMessage, conversationId, "98", str(timeStart), True, dbName)
            
            stop = datetime.datetime.now()
            totalTime="Total time used by OutputProcessor :" + str(stop - start)
            logger.info( "User Query: {0}".format(totalTime))
        except:
            logger.info( "User Query: {0}".format("Exception occurred in output processor"))
            raise
        return response
