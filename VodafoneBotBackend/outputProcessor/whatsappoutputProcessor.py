from PaxcelChatBotFramework import OutputProcessor
from PaxcelChatBotFramework.Models import Response

class WhatsappOutputProcessor(OutputProcessor):
    @staticmethod
    def CanHandle(request) -> bool:
        return request.AdditionalData["source"] == "whatsApp"
    def Process(self, request):

        context = request.Context
        output = request.Output
        messageList = []
        message = ""
        for out in output:
            messageList.append(out.Message)

        message = " ".join(map(str, messageList))
        additionalData = request.AdditionalData
        response = Response()
        response.Data = message
        response.Context = context

        return response

