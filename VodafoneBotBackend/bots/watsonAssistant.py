from ibm_watson import AssistantV1
import logging
logger = logging.getLogger(__name__)
class Assistant:
    """ This is a class description"""

    def __init__(self, config):
        self.__assistant__ = AssistantV1(iam_apikey=config["iam_apikey"],

                                         version=config["version"],
                                         # iam_access_token= config.apiToken,
                                         url=config["url"])

    def Message(self, payload):

        try:
            print(payload["workspace_id"])
            response = self.__assistant__.message(workspace_id=payload["workspace_id"],
                                                  input=payload["input"],
                                                  context=payload["context"]
                                                  ).get_result()
            print(response)
            print('bheem testing')
            logger.info(
                "User Query: {0}".format(response),
                extra=response
            )
        except:
            logger.error(
                "User Query: {0}".format(('error from watson end')),
                extra=response
            )


        return response
