import requests
import json
import psycopg2
import logging
import datetime

from .dbConnectionPooling import getConnection, putConnection
logger = logging.getLogger(__name__)



def getRules(parentId,dbName):
    return ExecuteQuery(dbName,"select * from getrules(%s);", [parentId])

def getRuleById(Id,dbName):
    return ExecuteQuery(dbName, "select * from getrulebyid(%s);", [Id])[0]


def getAnyThingElseNode(dbName,message,uuid,logId   ):
    billerId = "98"
    return ExecuteQuery(dbName,"select * from getanythingelsenode(%s,%s,%s,%s)", [message,int(logId),str(uuid),billerId])[0]

# getAnyThingElseNode('vodafone_bot','anything else','1234','1234')
def hasChildNode(parentId,dbName):
    return ExecuteQuery(dbName,"select * from hasChildNode(%s);", [parentId])[0]


def conversationHistory(conversationID):
    dbName="vodafone_bot"
    return ExecuteQuery(dbName, "select * from get_conversationhistory(%s);",[conversationID])[0]

def ExecuteQuery(dbName, query, parms):
    try:       
        start = datetime.datetime.now()
        connection = getConnection(dbName)
        conn = connection.cursor()
        conn.execute(query, parms)
        result = [item[0] for item in conn.fetchall()]
        stop = datetime.datetime.now()
        totalTime="Total time used by excutequery :" + str(stop - start)
        logger.info(
        "User Query: {0}".format(totalTime)
        )
        connection.commit()
    except (Exception, psycopg2.Error) as error:
        raise error
    finally:
        putConnection(connection, dbName)
    return result

# def userConversationTimeDetails(uuid,userid,convstarttime,dbName):
#     return ExecuteQuery(dbName,"select * from insert_userconversationtimedetails(%s,%s,%s)", [uuid,userid,convstarttime])
#
# def messagesChatlog(userMessage,dbName):
#     return ExecuteQuery(dbName,"select * from insert_messages(%s)", [userMessage])
#
# def messagesSequenceChatlogs(chatlogsId,msgId,isbot,dbName):
#     return ExecuteQuery(dbName,"select * from insert_sequencedetails(%s,%s,%s)", [chatlogsId,msgId,isbot])


def insertCHatlogs(userMessage,uuid,userid,convstarttime,isbot,dbName):
    chatlogsId = ExecuteQuery(dbName, "select * from insert_conversation_sequence(%s,%s,%s,%s,%s)",
                              [userMessage,uuid, userid, convstarttime,isbot])

    return chatlogsId

def insertFeedback(conversationidvalue,billervalue,convstarttime,feedbackdata,dbName):
    chatlogsId = ExecuteQuery(dbName, "select * from insert_feedback_conversation(%s,%s,%s,%s)", [conversationidvalue, billervalue, convstarttime, feedbackdata])
    return chatlogsId




