import psycopg2
from psycopg2 import pool
__connection__ = {}

def getPool(dbName, createIfNotExists = True):
    if dbName not in __connection__ and createIfNotExists:
        connPool = psycopg2.pool.ThreadedConnectionPool(5, 100,
                        user="paxcel",
                        password="N%by0M!bQ$yTR",
                        host = "paxcelpostgresql.cgbj2pjzbiuj.us-east-1.rds.amazonaws.com",
                        database= dbName)
        __connection__[dbName] = connPool

    return __connection__.get(dbName, None)
    

def getConnection(dbName):
    pool = getPool(dbName)
    return pool.getconn()

def putConnection(conn, dbName):
    pool = getPool(dbName, False)
    if pool == None:
        return
    pool.putconn(conn)
    