from .webInputProcessor import WebInputProcessor
from .whatsAppInputProcessor import WhatsAppInputProcessor

__all__ = [
    "WebInputProcessor",
    "WhatsAppInputProcessor"
]