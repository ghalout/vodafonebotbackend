from PaxcelChatBotFramework import InputProcessor
from PaxcelChatBotFramework.Models import Request

class WhatsAppInputProcessor(InputProcessor):

    @staticmethod
    def CanHandle(request) -> bool:

        return request["additionalData"]["source"] == "whatsApp"

    def Process(self, request):
        return Request(request)
