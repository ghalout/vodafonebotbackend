from PaxcelChatBotFramework import InputProcessor
from PaxcelChatBotFramework.Models import Request
from dataAccessLayer.dbHandler import insertCHatlogs
import logging
import json
import datetime
import random

logger = logging.getLogger(__name__)
class WebInputProcessor(InputProcessor):
    def __init__(self):
        self.type = ""
        self.input = {}
        self.additionalData = ""
        self.context = {}

    @staticmethod
    def CanHandle(request) -> bool:

        return request["additionalData"]["source"] == "web"

    def Process(self, request):
        start = datetime.datetime.now()
        received_json_data = request
        sresult = 'step1' + json.dumps(received_json_data)
        logger.info(
			"User Query: {0}".format(sresult )
		)
        print(received_json_data, "testing%%%%%%%%%%%%%%%%")
        dbName = received_json_data['additionalData']['dbName']

        if "message" in received_json_data['type']:
            inputMessage = received_json_data['input']['message']
        else:
            if '@text' in received_json_data['input']['data']:
                inputMessage = received_json_data['input']['data']['@text']
            else:
                inputMessage = received_json_data['input']['data']['#Name']



        if "conversationId" in received_json_data["context"]:
            conversationId = received_json_data["context"]["conversationId"]
        else:
            value = random.random()
            received_json_data["context"]["conversationId"] = str(value)
            conversationId = str(value)

        convstarttime = datetime.datetime.now()
        chatlogsId = insertCHatlogs(inputMessage, conversationId, "98", str(convstarttime), False, dbName)
        request["context"]["logsId"] = chatlogsId[0]

        self.type = received_json_data["type"]
        self.input = received_json_data["input"]
        self.additionalData = received_json_data["additionalData"]
        self.additionalData["message"] = received_json_data["input"]
        self.additionalData["logId"] = received_json_data["context"]["logsId"]
        #self.additionalData["workspaceIds"]["AGENT"]="976473b2-172e-4c9f-8d1e-7322ad76983f"
        self.additionalData["uuid"] = received_json_data["context"]["conversationId"]
        self.context = received_json_data["context"]

        request = {"type":self.type , "context": self.context, "additionalData": self.additionalData,
					  "input": self.input}
        Result=Request(request)

        stop = datetime.datetime.now()
        totalTime="Total time for process input :" + str(stop - start)
        logger.info("User Query: {0}".format(totalTime))
        logger.info("User Query: {0}".format(('result got from web Input Processor')))
        return Result
