from PaxcelChatBotFramework import Program, ComponentManager
from inputProcessor import WebInputProcessor, WhatsAppInputProcessor
from outputProcessor import WebOutputProcessor, WhatsappOutputProcessor
from bots import TemplateBot, NLPBot
from decisionEngine import DecisionEngine

def setup():
    manager = ComponentManager()
    manager.registerInputProcessor(WebInputProcessor)
    manager.registerInputProcessor(WhatsAppInputProcessor)
    manager.registerBot(TemplateBot)

    NLPBot.Initialize({
        'iam_apikey': 'KRWicLD7r3wOITsD8GZyLSZVj7uSz6bpV92UYuKDPlCs',
        'url': 'https://gateway-lon.watsonplatform.net/assistant/api',
        'version': '2018-06-12',
    })

    
    manager.registerBot(NLPBot)
    DecisionEngine.Initialize()
    manager.registerRuleEngine(DecisionEngine)
    manager.registerOutputProcessor(WebOutputProcessor)
    manager.registerOutputProcessor(WhatsappOutputProcessor)
    Program.initialize(manager)