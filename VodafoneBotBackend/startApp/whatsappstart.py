from PaxcelChatBotFramework import Program

from django.views.decorators.csrf import csrf_exempt
import json
@csrf_exempt
def processWhatsAppRequest(request):


    p = Program()
    data = eval(request.POST['data'])
    userInput = data['text']
    fromNumber = data['from']
    responsedata = {}
    if data['event'] == 'INBOX':
        request = {
            "type": "message",
            "input": {
                "type": "text",
                "text": userInput
            },
            "context": {},
            "additionalData": {"source": "whatsApp"}
        }
        outPutDict = p.Process(request)
        responsedata = {"apiwha_autoreply": outPutDict.Data}




    return HttpResponse(json.dumps(responsedata, sort_keys=False, indent=4, separators=(',', ': ')),
                        content_type='application/json', charset='UTF-8')
