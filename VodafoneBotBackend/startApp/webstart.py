from PaxcelChatBotFramework import Program
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
import json
from dataAccessLayer.dbHandler import insertFeedback, conversationHistory
import datetime
import logging
import random

logger = logging.getLogger(__name__)


@csrf_exempt
def processWebRequest(request):
    start = datetime.datetime.now()
    request = dict(json.loads(request.body))
    p = Program()
    outputDict = p.Process(request)
    jsonOutput = json.dumps(
        outputDict.__json__(), sort_keys=True, indent=2, separators=(",", ": ")
    )

    result = HttpResponse(jsonOutput, content_type="application/json", charset="UTF-8")

    stop = datetime.datetime.now()
    totalTime = "Total time used by backend :" + str(stop - start)
    logger.info("User Query: {0}".format(totalTime))
    return result


@csrf_exempt
def feedbackByConversation(request):
    start = datetime.datetime.now()

    request = dict(json.loads(request.body))

    dbName = request["additionalData"]["dbName"]
    if "conversationId" in request["context"]:
        uuid = request["context"]["conversationId"]
    else:
        value = random.random()
        request["context"]["conversationId"] = str(value)
        uuid = str(value)

    feedback = request["input"]["text"]
    billervalue = "98"

    chatlogsId = insertFeedback(uuid, billervalue, str(start), feedback, dbName)
    request["context"]["logsId"] = chatlogsId[0]

    result = HttpResponse(request, content_type="application/json", charset="UTF-8")
    stop = datetime.datetime.now()
    totalTime = "Total time used by backend :" + str(stop - start)
    logger.info("User Query: {0}".format(totalTime))
    return result

@csrf_exempt
def getChatHistory(request):
    request = dict(json.loads(request.body))
    print(request)
    conversationID = request["convId"]
    data = conversationHistory(conversationID)
    print(data)
    result = HttpResponse(json.dumps(data), content_type="application/json", charset="UTF-8")
    print ("result is=  ",result)
    return result

# @csrf_exempt
# def addToFavourite(request):
#     request = dict(json.loads(request.body))
#
#     url = 'http://100.25.71.230:3005/api/student/addToFavouriteScholarships'
#     email= context["$email"][0],
#     scholarshipId=context["$scholarshipId"][0],
#     params = {
#     "email":email,scholarshipId
#     }
#     headers =headerValue
#     response = requests.post(url, data=json.dumps(params), headers=headers)
#
#     "scholarshipId":
#     apidata = response.json()
#     data = {
#
#         "$favourites":apidata["data"],
#
#     }
#     print(data)
#     return data



# @csrf_exempt
# def addToFavourite(request):
#     request = dict(json.loads(request.body))
#
#     url = 'http://100.25.71.230:3005/api/student/addToFavouriteScholarships'
